//
//  AppDelegate.swift
//  WebNotifier
//
//  Created by Sergey Dikovitsky on 12/20/17.
//  Copyright © 2017 Sergey Dikovitsky. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    setupViewHierarchy()
    return true
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    LocalNotificationManager.shared.registerForLocalNotifications()
  }

}

private extension AppDelegate {

  func setupViewHierarchy() {
    let window = UIWindow(frame: UIScreen.main.bounds)
    window.rootViewController = WebNotifierViewController.createInNavigation()
    window.makeKeyAndVisible()
    self.window = window
  }

}
