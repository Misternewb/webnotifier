//
//  WebNotifierViewModel.swift
//  WebNotifier
//
//  Created by Sergey Dikovitsky on 12/20/17.
//  Copyright © 2017 Sergey Dikovitsky. All rights reserved.
//

import Foundation
import WebKit

private enum WebApplicationConstants {

  static let form = "web_form"
  static let type = "html"

}

class WebNotifierViewModel {

  enum NotificationPostError: Error {
    case empty
    case permissionDenied

    var localizedDescription: String {
      switch self {
      case .empty: return "Notification message should not be empty."
      case .permissionDenied: return "Notifications permission denied, please enable it in settings."
      }
    }

  }

  func webApplicationURL() -> URL? {
    return Bundle.main.url(forResource: WebApplicationConstants.form,
                           withExtension: WebApplicationConstants.type)
  }

  func postNotification(with message: String) throws {
    guard message.count > 0 else {
      throw NotificationPostError.empty
    }

    guard LocalNotificationManager.shared.isPermissionGranted else {
      throw NotificationPostError.permissionDenied
    }

    let notification = NotificationModel(message: message)
    LocalNotificationManager.shared.postNotification(with: notification)
  }

}
