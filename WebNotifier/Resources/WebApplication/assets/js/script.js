function notifyNativeApplicationOnFormSubmit() {
  window.webkit.messageHandlers.submitObserver.postMessage(inputValue());
}

function clearInput() {
  inputElement().value = "";
}

function inputValue() {
  return inputElement().value;
}

function inputElement() {
  return document.getElementById("notification-input");
}

function showSuccess(message) {
  flashAlert(message, 5000, "success");
}

function showError(message) {
  flashAlert(message, 5000, "error");
}

function flashAlert(message, duration, type) {
  var id = "top-message";
  var previousAlert = document.getElementById(id);
  if (previousAlert) {
    previousAlert.remove();
  }
  var alert = document.createElement("div");
  alert.className = "alert alert-" + type;
  alert.setAttribute("id", id);
  alert.innerHTML = message;
  setTimeout(function() {
    alert.parentNode.removeChild(alert);
  }, duration);
  document.body.insertBefore(alert, document.body.firstChild);
}

