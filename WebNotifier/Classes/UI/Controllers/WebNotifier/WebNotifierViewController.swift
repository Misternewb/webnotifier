//
//  WebNotifierViewController.swift
//  WebNotifier
//
//  Created by Sergey Dikovitsky on 12/20/17.
//  Copyright © 2017 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class WebNotifierViewController: BaseViewController {

  fileprivate let viewModel = WebNotifierViewModel()

  fileprivate lazy var webView: WKWebView = {
    let configuration = WKWebViewConfiguration()
    let contentController = WKUserContentController()
    contentController.add(self, name: "submitObserver")
    configuration.userContentController = contentController
    let webView = WKWebView(frame: .zero, configuration: configuration)
    webView.scrollView.isScrollEnabled = true
    webView.scrollView.bounces = false
    webView.allowsBackForwardNavigationGestures = false
    return webView
  }()

  override func loadView() {
    super.loadView()

    view.addSubview(webView)
    webView.navigationDelegate = self
    webView.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Web Notifier"

    loadWebApplication()
  }

  private func loadWebApplication() {
    guard let fileURL = viewModel.webApplicationURL() else {
      return
    }
    webView.loadFileURL(fileURL, allowingReadAccessTo: fileURL.deletingLastPathComponent())
  }

}

extension WebNotifierViewController: WKNavigationDelegate {

  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    let policy: WKNavigationActionPolicy
    switch navigationAction.navigationType {
    case .formResubmitted, .formSubmitted:
      policy = .cancel
    default:
      policy = .allow
    }
    decisionHandler(policy)
  }

}

extension WebNotifierViewController: WKScriptMessageHandler {

  func userContentController(_ userContentController: WKUserContentController,
                             didReceive message: WKScriptMessage) {
    let notificationMessage = (message.body as? String) ?? ""

    do {
      try viewModel.postNotification(with: notificationMessage)
      showSuccess(with: "You have successfully posted a notification!")
      clearNotificationInput()
    } catch let error as WebNotifierViewModel.NotificationPostError {
      showError(with: error.localizedDescription)
    } catch {
      showError(with: error.localizedDescription)
    }
  }

}

private extension WebNotifierViewController {

  func clearNotificationInput() {
    webView.evaluateJavaScript("clearInput()", completionHandler: nil)
  }

  func showSuccess(with message: String) {
    webView.evaluateJavaScript("showSuccess(\"\(message)\")", completionHandler: nil)
  }

  func showError(with message: String) {
    webView.evaluateJavaScript("showError(\"\(message)\")", completionHandler: nil)
  }

}
