//
//  BaseViewController.swift
//  WebNotifier
//
//  Created by Sergey Dikovitsky on 12/20/17.
//  Copyright © 2017 Sergey Dikovitsky. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

  class func createInNavigation() -> UINavigationController {
    return UINavigationController(rootViewController: self.init())
  }

  required init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }

  override func loadView() {
    super.loadView()

    view.backgroundColor = .white
  }

}
