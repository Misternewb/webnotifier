//
//  LocalNotificationManager.swift
//  WebNotifier
//
//  Created by Sergey Dikovitsky on 12/20/17.
//  Copyright © 2017 Sergey Dikovitsky. All rights reserved.
//

import UIKit
import UserNotifications

class LocalNotificationManager: NSObject {

  static let shared = LocalNotificationManager()

  private let notificationCenter = UNUserNotificationCenter.current()
  var isPermissionGranted = false

  private override init() {
    super.init()
    notificationCenter.delegate = self
  }

  func registerForLocalNotifications() {
    notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] granted, _ in
      self?.isPermissionGranted = granted
    }
  }

  func postNotification(with model: NotificationModel) {
    let content = UNMutableNotificationContent()
    content.body = model.message
    content.sound = UNNotificationSound.default()

    let request = UNNotificationRequest(identifier: "LocalMessageNotification",
                                        content: content,
                                        trigger: nil)
    notificationCenter.add(request)
  }

}

extension LocalNotificationManager: UNUserNotificationCenterDelegate {

  // swiftlint:disable:next line_length
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler(.alert)
  }

}
