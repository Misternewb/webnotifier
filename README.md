# Web Notifier
Web Notifier is an iOS aplication, which features:

  - unbelievable local notification posting
  - incredible design and color scheme
### Tech information
  - iOS 11+
  - Swift 4.0
  - Xcode 9.2+
  - [Code style](https://github.com/raywenderlich/swift-style-guide)
  - For better readabilty, it is advised to use 2 spaces length tabs/indents
### Installation

Clone or download source code.
Install CocoaPods dependencies.
```sh
$ cd path/to/project
$ pod install
```

